// Code generated by goa v3.2.4, DO NOT EDIT.
//
// test HTTP server types
//
// Command:
// $ goa gen gitlab.com/fieldkit/cloud/server/api/design

package server

import (
	test "gitlab.com/fieldkit/cloud/server/api/gen/test"
	goa "goa.design/goa/v3/pkg"
)

// NoopForbiddenResponseBody is the type of the "test" service "noop" endpoint
// HTTP response body for the "forbidden" error.
type NoopForbiddenResponseBody struct {
	// Name is the name of this class of errors.
	Name string `form:"name" json:"name" xml:"name"`
	// ID is a unique identifier for this particular occurrence of the problem.
	ID string `form:"id" json:"id" xml:"id"`
	// Message is a human-readable explanation specific to this occurrence of the
	// problem.
	Message string `form:"message" json:"message" xml:"message"`
	// Is the error temporary?
	Temporary bool `form:"temporary" json:"temporary" xml:"temporary"`
	// Is the error a timeout?
	Timeout bool `form:"timeout" json:"timeout" xml:"timeout"`
	// Is the error a server-side fault?
	Fault bool `form:"fault" json:"fault" xml:"fault"`
}

// NoopNotFoundResponseBody is the type of the "test" service "noop" endpoint
// HTTP response body for the "not-found" error.
type NoopNotFoundResponseBody struct {
	// Name is the name of this class of errors.
	Name string `form:"name" json:"name" xml:"name"`
	// ID is a unique identifier for this particular occurrence of the problem.
	ID string `form:"id" json:"id" xml:"id"`
	// Message is a human-readable explanation specific to this occurrence of the
	// problem.
	Message string `form:"message" json:"message" xml:"message"`
	// Is the error temporary?
	Temporary bool `form:"temporary" json:"temporary" xml:"temporary"`
	// Is the error a timeout?
	Timeout bool `form:"timeout" json:"timeout" xml:"timeout"`
	// Is the error a server-side fault?
	Fault bool `form:"fault" json:"fault" xml:"fault"`
}

// NoopBadRequestResponseBody is the type of the "test" service "noop" endpoint
// HTTP response body for the "bad-request" error.
type NoopBadRequestResponseBody struct {
	// Name is the name of this class of errors.
	Name string `form:"name" json:"name" xml:"name"`
	// ID is a unique identifier for this particular occurrence of the problem.
	ID string `form:"id" json:"id" xml:"id"`
	// Message is a human-readable explanation specific to this occurrence of the
	// problem.
	Message string `form:"message" json:"message" xml:"message"`
	// Is the error temporary?
	Temporary bool `form:"temporary" json:"temporary" xml:"temporary"`
	// Is the error a timeout?
	Timeout bool `form:"timeout" json:"timeout" xml:"timeout"`
	// Is the error a server-side fault?
	Fault bool `form:"fault" json:"fault" xml:"fault"`
}

// NoopUnauthorizedResponseBody is the type of the "test" service "noop"
// endpoint HTTP response body for the "unauthorized" error.
type NoopUnauthorizedResponseBody string

// NewNoopForbiddenResponseBody builds the HTTP response body from the result
// of the "noop" endpoint of the "test" service.
func NewNoopForbiddenResponseBody(res *goa.ServiceError) *NoopForbiddenResponseBody {
	body := &NoopForbiddenResponseBody{
		Name:      res.Name,
		ID:        res.ID,
		Message:   res.Message,
		Temporary: res.Temporary,
		Timeout:   res.Timeout,
		Fault:     res.Fault,
	}
	return body
}

// NewNoopNotFoundResponseBody builds the HTTP response body from the result of
// the "noop" endpoint of the "test" service.
func NewNoopNotFoundResponseBody(res *goa.ServiceError) *NoopNotFoundResponseBody {
	body := &NoopNotFoundResponseBody{
		Name:      res.Name,
		ID:        res.ID,
		Message:   res.Message,
		Temporary: res.Temporary,
		Timeout:   res.Timeout,
		Fault:     res.Fault,
	}
	return body
}

// NewNoopBadRequestResponseBody builds the HTTP response body from the result
// of the "noop" endpoint of the "test" service.
func NewNoopBadRequestResponseBody(res *goa.ServiceError) *NoopBadRequestResponseBody {
	body := &NoopBadRequestResponseBody{
		Name:      res.Name,
		ID:        res.ID,
		Message:   res.Message,
		Temporary: res.Temporary,
		Timeout:   res.Timeout,
		Fault:     res.Fault,
	}
	return body
}

// NewNoopUnauthorizedResponseBody builds the HTTP response body from the
// result of the "noop" endpoint of the "test" service.
func NewNoopUnauthorizedResponseBody(res test.Unauthorized) NoopUnauthorizedResponseBody {
	body := NoopUnauthorizedResponseBody(res)
	return body
}
