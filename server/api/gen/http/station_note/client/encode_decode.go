// Code generated by goa v3.2.4, DO NOT EDIT.
//
// station_note HTTP client encoders and decoders
//
// Command:
// $ goa gen gitlab.com/fieldkit/cloud/server/api/design

package client

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	stationnote "gitlab.com/fieldkit/cloud/server/api/gen/station_note"
	stationnoteviews "gitlab.com/fieldkit/cloud/server/api/gen/station_note/views"
	goahttp "goa.design/goa/v3/http"
)

// BuildStationRequest instantiates a HTTP request object with method and path
// set to call the "station_note" service "station" endpoint
func (c *Client) BuildStationRequest(ctx context.Context, v interface{}) (*http.Request, error) {
	var (
		stationID int32
	)
	{
		p, ok := v.(*stationnote.StationPayload)
		if !ok {
			return nil, goahttp.ErrInvalidType("station_note", "station", "*stationnote.StationPayload", v)
		}
		stationID = p.StationID
	}
	u := &url.URL{Scheme: c.scheme, Host: c.host, Path: StationStationNotePath(stationID)}
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, goahttp.ErrInvalidURL("station_note", "station", u.String(), err)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return req, nil
}

// EncodeStationRequest returns an encoder for requests sent to the
// station_note station server.
func EncodeStationRequest(encoder func(*http.Request) goahttp.Encoder) func(*http.Request, interface{}) error {
	return func(req *http.Request, v interface{}) error {
		p, ok := v.(*stationnote.StationPayload)
		if !ok {
			return goahttp.ErrInvalidType("station_note", "station", "*stationnote.StationPayload", v)
		}
		if p.Auth != nil {
			head := *p.Auth
			if !strings.Contains(head, " ") {
				req.Header.Set("Authorization", "Bearer "+head)
			} else {
				req.Header.Set("Authorization", head)
			}
		}
		return nil
	}
}

// DecodeStationResponse returns a decoder for responses returned by the
// station_note station endpoint. restoreBody controls whether the response
// body should be restored after having been read.
// DecodeStationResponse may return the following errors:
//   - "unauthorized" (type *goa.ServiceError): http.StatusUnauthorized
//   - "forbidden" (type *goa.ServiceError): http.StatusForbidden
//   - "not-found" (type *goa.ServiceError): http.StatusNotFound
//   - "bad-request" (type *goa.ServiceError): http.StatusBadRequest
//   - error: internal error
func DecodeStationResponse(decoder func(*http.Response) goahttp.Decoder, restoreBody bool) func(*http.Response) (interface{}, error) {
	return func(resp *http.Response) (interface{}, error) {
		if restoreBody {
			b, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			defer func() {
				resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			}()
		} else {
			defer resp.Body.Close()
		}
		switch resp.StatusCode {
		case http.StatusOK:
			var (
				body StationResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "station", err)
			}
			p := NewStationNotesViewOK(&body)
			view := "default"
			vres := &stationnoteviews.StationNotes{Projected: p, View: view}
			if err = stationnoteviews.ValidateStationNotes(vres); err != nil {
				return nil, goahttp.ErrValidationError("station_note", "station", err)
			}
			res := stationnote.NewStationNotes(vres)
			return res, nil
		case http.StatusUnauthorized:
			var (
				body StationUnauthorizedResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "station", err)
			}
			err = ValidateStationUnauthorizedResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "station", err)
			}
			return nil, NewStationUnauthorized(&body)
		case http.StatusForbidden:
			var (
				body StationForbiddenResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "station", err)
			}
			err = ValidateStationForbiddenResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "station", err)
			}
			return nil, NewStationForbidden(&body)
		case http.StatusNotFound:
			var (
				body StationNotFoundResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "station", err)
			}
			err = ValidateStationNotFoundResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "station", err)
			}
			return nil, NewStationNotFound(&body)
		case http.StatusBadRequest:
			var (
				body StationBadRequestResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "station", err)
			}
			err = ValidateStationBadRequestResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "station", err)
			}
			return nil, NewStationBadRequest(&body)
		default:
			body, _ := ioutil.ReadAll(resp.Body)
			return nil, goahttp.ErrInvalidResponse("station_note", "station", resp.StatusCode, string(body))
		}
	}
}

// BuildAddNoteRequest instantiates a HTTP request object with method and path
// set to call the "station_note" service "add note" endpoint
func (c *Client) BuildAddNoteRequest(ctx context.Context, v interface{}) (*http.Request, error) {
	var (
		stationID int32
	)
	{
		p, ok := v.(*stationnote.AddNotePayload)
		if !ok {
			return nil, goahttp.ErrInvalidType("station_note", "add note", "*stationnote.AddNotePayload", v)
		}
		stationID = p.StationID
	}
	u := &url.URL{Scheme: c.scheme, Host: c.host, Path: AddNoteStationNotePath(stationID)}
	req, err := http.NewRequest("POST", u.String(), nil)
	if err != nil {
		return nil, goahttp.ErrInvalidURL("station_note", "add note", u.String(), err)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return req, nil
}

// EncodeAddNoteRequest returns an encoder for requests sent to the
// station_note add note server.
func EncodeAddNoteRequest(encoder func(*http.Request) goahttp.Encoder) func(*http.Request, interface{}) error {
	return func(req *http.Request, v interface{}) error {
		p, ok := v.(*stationnote.AddNotePayload)
		if !ok {
			return goahttp.ErrInvalidType("station_note", "add note", "*stationnote.AddNotePayload", v)
		}
		{
			head := p.Auth
			if !strings.Contains(head, " ") {
				req.Header.Set("Authorization", "Bearer "+head)
			} else {
				req.Header.Set("Authorization", head)
			}
		}
		body := NewAddNoteRequestBody(p)
		if err := encoder(req).Encode(&body); err != nil {
			return goahttp.ErrEncodingError("station_note", "add note", err)
		}
		return nil
	}
}

// DecodeAddNoteResponse returns a decoder for responses returned by the
// station_note add note endpoint. restoreBody controls whether the response
// body should be restored after having been read.
// DecodeAddNoteResponse may return the following errors:
//   - "unauthorized" (type *goa.ServiceError): http.StatusUnauthorized
//   - "forbidden" (type *goa.ServiceError): http.StatusForbidden
//   - "not-found" (type *goa.ServiceError): http.StatusNotFound
//   - "bad-request" (type *goa.ServiceError): http.StatusBadRequest
//   - error: internal error
func DecodeAddNoteResponse(decoder func(*http.Response) goahttp.Decoder, restoreBody bool) func(*http.Response) (interface{}, error) {
	return func(resp *http.Response) (interface{}, error) {
		if restoreBody {
			b, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			defer func() {
				resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			}()
		} else {
			defer resp.Body.Close()
		}
		switch resp.StatusCode {
		case http.StatusOK:
			var (
				body AddNoteResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "add note", err)
			}
			p := NewAddNoteStationNoteOK(&body)
			view := "default"
			vres := &stationnoteviews.StationNote{Projected: p, View: view}
			if err = stationnoteviews.ValidateStationNote(vres); err != nil {
				return nil, goahttp.ErrValidationError("station_note", "add note", err)
			}
			res := stationnote.NewStationNote(vres)
			return res, nil
		case http.StatusUnauthorized:
			var (
				body AddNoteUnauthorizedResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "add note", err)
			}
			err = ValidateAddNoteUnauthorizedResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "add note", err)
			}
			return nil, NewAddNoteUnauthorized(&body)
		case http.StatusForbidden:
			var (
				body AddNoteForbiddenResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "add note", err)
			}
			err = ValidateAddNoteForbiddenResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "add note", err)
			}
			return nil, NewAddNoteForbidden(&body)
		case http.StatusNotFound:
			var (
				body AddNoteNotFoundResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "add note", err)
			}
			err = ValidateAddNoteNotFoundResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "add note", err)
			}
			return nil, NewAddNoteNotFound(&body)
		case http.StatusBadRequest:
			var (
				body AddNoteBadRequestResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "add note", err)
			}
			err = ValidateAddNoteBadRequestResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "add note", err)
			}
			return nil, NewAddNoteBadRequest(&body)
		default:
			body, _ := ioutil.ReadAll(resp.Body)
			return nil, goahttp.ErrInvalidResponse("station_note", "add note", resp.StatusCode, string(body))
		}
	}
}

// BuildUpdateNoteRequest instantiates a HTTP request object with method and
// path set to call the "station_note" service "update note" endpoint
func (c *Client) BuildUpdateNoteRequest(ctx context.Context, v interface{}) (*http.Request, error) {
	var (
		stationID     int32
		stationNoteID int32
	)
	{
		p, ok := v.(*stationnote.UpdateNotePayload)
		if !ok {
			return nil, goahttp.ErrInvalidType("station_note", "update note", "*stationnote.UpdateNotePayload", v)
		}
		stationID = p.StationID
		stationNoteID = p.StationNoteID
	}
	u := &url.URL{Scheme: c.scheme, Host: c.host, Path: UpdateNoteStationNotePath(stationID, stationNoteID)}
	req, err := http.NewRequest("POST", u.String(), nil)
	if err != nil {
		return nil, goahttp.ErrInvalidURL("station_note", "update note", u.String(), err)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return req, nil
}

// EncodeUpdateNoteRequest returns an encoder for requests sent to the
// station_note update note server.
func EncodeUpdateNoteRequest(encoder func(*http.Request) goahttp.Encoder) func(*http.Request, interface{}) error {
	return func(req *http.Request, v interface{}) error {
		p, ok := v.(*stationnote.UpdateNotePayload)
		if !ok {
			return goahttp.ErrInvalidType("station_note", "update note", "*stationnote.UpdateNotePayload", v)
		}
		{
			head := p.Auth
			if !strings.Contains(head, " ") {
				req.Header.Set("Authorization", "Bearer "+head)
			} else {
				req.Header.Set("Authorization", head)
			}
		}
		body := NewUpdateNoteRequestBody(p)
		if err := encoder(req).Encode(&body); err != nil {
			return goahttp.ErrEncodingError("station_note", "update note", err)
		}
		return nil
	}
}

// DecodeUpdateNoteResponse returns a decoder for responses returned by the
// station_note update note endpoint. restoreBody controls whether the response
// body should be restored after having been read.
// DecodeUpdateNoteResponse may return the following errors:
//   - "unauthorized" (type *goa.ServiceError): http.StatusUnauthorized
//   - "forbidden" (type *goa.ServiceError): http.StatusForbidden
//   - "not-found" (type *goa.ServiceError): http.StatusNotFound
//   - "bad-request" (type *goa.ServiceError): http.StatusBadRequest
//   - error: internal error
func DecodeUpdateNoteResponse(decoder func(*http.Response) goahttp.Decoder, restoreBody bool) func(*http.Response) (interface{}, error) {
	return func(resp *http.Response) (interface{}, error) {
		if restoreBody {
			b, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			defer func() {
				resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			}()
		} else {
			defer resp.Body.Close()
		}
		switch resp.StatusCode {
		case http.StatusOK:
			var (
				body UpdateNoteResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "update note", err)
			}
			p := NewUpdateNoteStationNoteOK(&body)
			view := "default"
			vres := &stationnoteviews.StationNote{Projected: p, View: view}
			if err = stationnoteviews.ValidateStationNote(vres); err != nil {
				return nil, goahttp.ErrValidationError("station_note", "update note", err)
			}
			res := stationnote.NewStationNote(vres)
			return res, nil
		case http.StatusUnauthorized:
			var (
				body UpdateNoteUnauthorizedResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "update note", err)
			}
			err = ValidateUpdateNoteUnauthorizedResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "update note", err)
			}
			return nil, NewUpdateNoteUnauthorized(&body)
		case http.StatusForbidden:
			var (
				body UpdateNoteForbiddenResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "update note", err)
			}
			err = ValidateUpdateNoteForbiddenResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "update note", err)
			}
			return nil, NewUpdateNoteForbidden(&body)
		case http.StatusNotFound:
			var (
				body UpdateNoteNotFoundResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "update note", err)
			}
			err = ValidateUpdateNoteNotFoundResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "update note", err)
			}
			return nil, NewUpdateNoteNotFound(&body)
		case http.StatusBadRequest:
			var (
				body UpdateNoteBadRequestResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "update note", err)
			}
			err = ValidateUpdateNoteBadRequestResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "update note", err)
			}
			return nil, NewUpdateNoteBadRequest(&body)
		default:
			body, _ := ioutil.ReadAll(resp.Body)
			return nil, goahttp.ErrInvalidResponse("station_note", "update note", resp.StatusCode, string(body))
		}
	}
}

// BuildDeleteNoteRequest instantiates a HTTP request object with method and
// path set to call the "station_note" service "delete note" endpoint
func (c *Client) BuildDeleteNoteRequest(ctx context.Context, v interface{}) (*http.Request, error) {
	var (
		stationID     int32
		stationNoteID int32
	)
	{
		p, ok := v.(*stationnote.DeleteNotePayload)
		if !ok {
			return nil, goahttp.ErrInvalidType("station_note", "delete note", "*stationnote.DeleteNotePayload", v)
		}
		stationID = p.StationID
		stationNoteID = p.StationNoteID
	}
	u := &url.URL{Scheme: c.scheme, Host: c.host, Path: DeleteNoteStationNotePath(stationID, stationNoteID)}
	req, err := http.NewRequest("DELETE", u.String(), nil)
	if err != nil {
		return nil, goahttp.ErrInvalidURL("station_note", "delete note", u.String(), err)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return req, nil
}

// EncodeDeleteNoteRequest returns an encoder for requests sent to the
// station_note delete note server.
func EncodeDeleteNoteRequest(encoder func(*http.Request) goahttp.Encoder) func(*http.Request, interface{}) error {
	return func(req *http.Request, v interface{}) error {
		p, ok := v.(*stationnote.DeleteNotePayload)
		if !ok {
			return goahttp.ErrInvalidType("station_note", "delete note", "*stationnote.DeleteNotePayload", v)
		}
		{
			head := p.Auth
			if !strings.Contains(head, " ") {
				req.Header.Set("Authorization", "Bearer "+head)
			} else {
				req.Header.Set("Authorization", head)
			}
		}
		return nil
	}
}

// DecodeDeleteNoteResponse returns a decoder for responses returned by the
// station_note delete note endpoint. restoreBody controls whether the response
// body should be restored after having been read.
// DecodeDeleteNoteResponse may return the following errors:
//   - "unauthorized" (type *goa.ServiceError): http.StatusUnauthorized
//   - "forbidden" (type *goa.ServiceError): http.StatusForbidden
//   - "not-found" (type *goa.ServiceError): http.StatusNotFound
//   - "bad-request" (type *goa.ServiceError): http.StatusBadRequest
//   - error: internal error
func DecodeDeleteNoteResponse(decoder func(*http.Response) goahttp.Decoder, restoreBody bool) func(*http.Response) (interface{}, error) {
	return func(resp *http.Response) (interface{}, error) {
		if restoreBody {
			b, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			defer func() {
				resp.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			}()
		} else {
			defer resp.Body.Close()
		}
		switch resp.StatusCode {
		case http.StatusNoContent:
			return nil, nil
		case http.StatusUnauthorized:
			var (
				body DeleteNoteUnauthorizedResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "delete note", err)
			}
			err = ValidateDeleteNoteUnauthorizedResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "delete note", err)
			}
			return nil, NewDeleteNoteUnauthorized(&body)
		case http.StatusForbidden:
			var (
				body DeleteNoteForbiddenResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "delete note", err)
			}
			err = ValidateDeleteNoteForbiddenResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "delete note", err)
			}
			return nil, NewDeleteNoteForbidden(&body)
		case http.StatusNotFound:
			var (
				body DeleteNoteNotFoundResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "delete note", err)
			}
			err = ValidateDeleteNoteNotFoundResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "delete note", err)
			}
			return nil, NewDeleteNoteNotFound(&body)
		case http.StatusBadRequest:
			var (
				body DeleteNoteBadRequestResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("station_note", "delete note", err)
			}
			err = ValidateDeleteNoteBadRequestResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("station_note", "delete note", err)
			}
			return nil, NewDeleteNoteBadRequest(&body)
		default:
			body, _ := ioutil.ReadAll(resp.Body)
			return nil, goahttp.ErrInvalidResponse("station_note", "delete note", resp.StatusCode, string(body))
		}
	}
}

// unmarshalStationNoteResponseBodyToStationnoteviewsStationNoteView builds a
// value of type *stationnoteviews.StationNoteView from a value of type
// *StationNoteResponseBody.
func unmarshalStationNoteResponseBodyToStationnoteviewsStationNoteView(v *StationNoteResponseBody) *stationnoteviews.StationNoteView {
	res := &stationnoteviews.StationNoteView{
		ID:        v.ID,
		CreatedAt: v.CreatedAt,
		UpdatedAt: v.UpdatedAt,
		Body:      v.Body,
	}
	res.Author = unmarshalStationNoteAuthorResponseBodyToStationnoteviewsStationNoteAuthorView(v.Author)

	return res
}

// unmarshalStationNoteAuthorResponseBodyToStationnoteviewsStationNoteAuthorView
// builds a value of type *stationnoteviews.StationNoteAuthorView from a value
// of type *StationNoteAuthorResponseBody.
func unmarshalStationNoteAuthorResponseBodyToStationnoteviewsStationNoteAuthorView(v *StationNoteAuthorResponseBody) *stationnoteviews.StationNoteAuthorView {
	res := &stationnoteviews.StationNoteAuthorView{
		ID:   v.ID,
		Name: v.Name,
	}
	if v.Photo != nil {
		res.Photo = unmarshalStationNoteAuthorPhotoResponseBodyToStationnoteviewsStationNoteAuthorPhotoView(v.Photo)
	}

	return res
}

// unmarshalStationNoteAuthorPhotoResponseBodyToStationnoteviewsStationNoteAuthorPhotoView
// builds a value of type *stationnoteviews.StationNoteAuthorPhotoView from a
// value of type *StationNoteAuthorPhotoResponseBody.
func unmarshalStationNoteAuthorPhotoResponseBodyToStationnoteviewsStationNoteAuthorPhotoView(v *StationNoteAuthorPhotoResponseBody) *stationnoteviews.StationNoteAuthorPhotoView {
	if v == nil {
		return nil
	}
	res := &stationnoteviews.StationNoteAuthorPhotoView{
		URL: v.URL,
	}

	return res
}
