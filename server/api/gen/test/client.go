// Code generated by goa v3.2.4, DO NOT EDIT.
//
// test client
//
// Command:
// $ goa gen gitlab.com/fieldkit/cloud/server/api/design

package test

import (
	"context"

	goa "goa.design/goa/v3/pkg"
)

// Client is the "test" service client.
type Client struct {
	NoopEndpoint goa.Endpoint
}

// NewClient initializes a "test" service client given the endpoints.
func NewClient(noop goa.Endpoint) *Client {
	return &Client{
		NoopEndpoint: noop,
	}
}

// Noop calls the "noop" endpoint of the "test" service.
func (c *Client) Noop(ctx context.Context) (err error) {
	_, err = c.NoopEndpoint(ctx, nil)
	return
}
