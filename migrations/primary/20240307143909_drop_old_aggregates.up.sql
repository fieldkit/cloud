DROP TABLE IF EXISTS fieldkit.aggregated_1h;
DROP TABLE IF EXISTS fieldkit.aggregated_1m;
DROP TABLE IF EXISTS fieldkit.aggregated_6h;
DROP TABLE IF EXISTS fieldkit.aggregated_10m;
DROP TABLE IF EXISTS fieldkit.aggregated_10s;
DROP TABLE IF EXISTS fieldkit.aggregated_12h;
DROP TABLE IF EXISTS fieldkit.aggregated_24h;
DROP TABLE IF EXISTS fieldkit.aggregated_30m;